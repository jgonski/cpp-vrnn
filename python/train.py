import torch.nn as nn
import torch.utils
import torch.utils.data
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt 
from model import VRNN
import numpy as np
import pickle
import h5py

import sys, os
from sklearn.metrics import roc_curve, auc

import time


"""implementation of the Variational Recurrent
Neural Network (VRNN) from https://arxiv.org/abs/1506.02216
using unimodal isotropic gaussian distributions for 
inference, prior, and generating models."""

dataDir = '/nevis/katya01/data/users/akahn/VRNN_Data/LHCOlympics/Output_h5/'

ptnorm = False
train_hlvs = False
#train_hlvs = True

proc = sys.argv[1]
topN = sys.argv[2]
sample = sys.argv[3]
maxconsts = int(sys.argv[4])
load = bool(int(sys.argv[5]))
kl_weight = float(sys.argv[6])
h_dim = int(sys.argv[7])
z_dim = int(sys.argv[8])
cuda_idx = int(sys.argv[9])
copynum = sys.argv[10]
#maxconsts = 20
#topN = sys.argv[2]
#sample = sys.argv[3]
suffix = "_DrCut"
#if(len(sys.argv) > 4):
#  suffix += "_"+sys.argv[4]
#if(len(sys.argv) > 5):
#  suffix += "_"+sys.argv[5]

if(train_hlvs):
  rocname = "lhco_train_"+sample+"_"+str(maxconsts)+"_"+proc+"_"+"top"+topN+suffix+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)
else:
  rocname = "lhco_train_"+sample+"_"+str(maxconsts)+"_"+proc+"_"+"top"+topN+suffix+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+"_constonly"

if not os.path.exists(sys.path[0]+"/plots/"+rocname):
  try:
    os.makedirs(sys.path[0]+"/plots/"+rocname)
    os.makedirs(sys.path[0]+"/plots/"+rocname+"/roc")
    os.makedirs(sys.path[0]+"/plots/"+rocname+"/scores")
  except OSError as exc: # Guard against race condition
    if exc.errno != errno.EEXIST:
      raise


def get_data(infile = None, n_const = None):
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_"+sample+"_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_"+sample+"_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.hdf5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_3P_"+sample+"_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_LP_"+sample+"_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_"+sample+"_VRNN_"+proc+"_copy"+copynum+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    data = dict()
    avg_jets = dict()
    hlvs = dict()
    vecs = dict()
    masses = dict()
    all_hlvs = []
    jetgroup = infile["jets/top"+topN]
    for n_c in jetgroup.keys():
      if(len(jetgroup[n_c+"/hlvs"][()]) > 0):
        print(n_c, np.shape(jetgroup[n_c+"/hlvs"]))
        for i in range(len(jetgroup[n_c+"/hlvs"][()])):
          all_hlvs.append(jetgroup[n_c+"/hlvs"][(i)])
          #print(n_c, i, jetgroup[n_c+"/hlvs"][(i)])
    all_hlvs = np.array(all_hlvs)
    hlv_means = np.mean(all_hlvs, axis=0)
    hlv_stds = np.std(all_hlvs, axis=0)
    for n_c in jetgroup.keys():
      if(len(jetgroup[n_c+"/constituents"][()]) > 0):
        tmp_consts = jetgroup[n_c+"/constituents"][()]
        if(ptnorm):
          for i in range(len(tmp_consts)):
            pt1 = tmp_consts[i][0][0]
            for j in range(int(n_c)):
              tmp_consts[i][j][0] /= pt1
        #data.update({n_c: jetgroup[n_c+"/constituents"]})
        #print(n_c, np.mean(tmp_consts, axis=0)[:,0][0])
        #print(n_c, np.mean(tmp_consts, axis=0)[0][0])
        data.update({n_c: tmp_consts})
        avg_jets.update({n_c: np.mean(tmp_consts, axis=0)[:,0]})
      if(len(jetgroup[n_c+"/hlvs"][()]) > 0):
        hlvs.update({n_c: (jetgroup[n_c+"/hlvs"][()] - hlv_means)/hlv_stds})
      else:
        hlvs.update({n_c: jetgroup[n_c+"/hlvs"]})
      vecs.update({n_c: jetgroup[n_c+"/4vecs"]}) 
      #masses.update({n_c: jetgroup[n_c+"/4vecs"][()][:,3]/1000.0}) #masses in TeV
    n_events = len(infile["events"].keys())
    return data, hlvs, hlv_means, hlv_stds, n_events, avg_jets, vecs, masses

def get_val_data(hlv_means, hlv_stds, infile = None, n_const = None):
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_Validation_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_3P_Validation_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_LP_Validation_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_Validation_VRNN_"+proc+"_copy"+copynum+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_LP_Validation_VRNN_"+proc+"_copy"+copynum+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    data = dict()
    hlvs = dict()
    vecs = dict()
    masses = dict()
    jetgroup = infile["jets/top"+topN]
    for n_c in jetgroup.keys():
      if(len(jetgroup[n_c+"/constituents"][()]) > 0):
        tmp_consts = jetgroup[n_c+"/constituents"][()]
        if(ptnorm):
          for i in range(len(tmp_consts)):
            pt1 = tmp_consts[i][0][0]
            for j in range(int(n_c)):
              tmp_consts[i][j][0] /= pt1
        #data.update({n_c: jetgroup[n_c+"/constituents"]})
        data.update({n_c: tmp_consts})
      if(len(jetgroup[n_c+"/hlvs"][()]) > 0):
        hlvs.update({n_c: (jetgroup[n_c+"/hlvs"][()] - hlv_means)/hlv_stds})
      else:
        hlvs.update({n_c: jetgroup[n_c+"/hlvs"]})
      vecs.update({n_c: jetgroup[n_c+"/4vecs"]}) 
      #masses.update({n_c: jetgroup[n_c+"/4vecs"][()][:,3]/1000.0}) #masses in TeV
    n_events = len(infile["events"].keys())
    return data, hlvs, n_events, vecs, masses

def get_anom_data(hlv_means, hlv_stds, infile = None, n_const = None):
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_Signal_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_3P_Signal_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_LP_Signal_VRNN_"+proc+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_Signal_VRNN_"+proc+"_copy"+copynum+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    #if(infile is None): infile = h5py.File(dataDir+"events_anomalydetection_LP_Signal_VRNN_"+proc+"_copy"+copynum+"_"+str(maxconsts)+"C"+suffix+"_preprocessed.h5", "r+")
    data = dict()
    hlvs = dict()
    vecs = dict()
    masses = dict()
    jetgroup = infile["jets/top"+topN]
    for n_c in jetgroup.keys():
      if(len(jetgroup[n_c+"/constituents"][()]) > 0):
        tmp_consts = jetgroup[n_c+"/constituents"][()]
        if(ptnorm):
          for i in range(len(tmp_consts)):
            pt1 = tmp_consts[i][0][0]
            for j in range(int(n_c)):
              tmp_consts[i][j][0] /= pt1
        #data.update({n_c: jetgroup[n_c+"/constituents"]})
        data.update({n_c: tmp_consts})
      if(len(jetgroup[n_c+"/hlvs"][()]) > 0):
        hlvs.update({n_c: (jetgroup[n_c+"/hlvs"][()] - hlv_means)/hlv_stds})
      else:
        hlvs.update({n_c: jetgroup[n_c+"/hlvs"]})
      vecs.update({n_c: jetgroup[n_c+"/4vecs"]}) 
      #masses.update({n_c: jetgroup[n_c+"/4vecs"][()][:,3]/1000.0}) #masses in TeV
    n_events = len(infile["events"].keys())
    return data, hlvs, n_events, vecs, masses


def get_training_event(n_event, hlv_means, hlv_stds, infile = None, n_const = None):
    if(infile is None): infile = h5py.File(dataDir+"Training_"+str(maxconsts)+"C_"+proc+"_"+str(maxconsts)+"C"+suffix+"_LHCO.h5", "r+")
    data = dict()
    hlvs = dict()
    #Get event
    event = infile["events/"+str(n_event)]
    #Loop through jets, up to topN
    for n_jet in range(min(len(event.keys()), int(topN))):
      #Add jet to event
      data.update({n_jet: event["jet"+str(n_jet)+"/constituents"][()]})
      hlvs.update({n_jet: (event["jet"+str(n_jet)+"/hlvs"][()] - hlv_means)/hlv_stds})
    return data, hlvs


def get_val_event(n_event, hlv_means, hlv_stds, infile = None, n_const = None):
    if(infile is None): infile = h5py.File(dataDir+"Validation_"+str(maxconsts)+"C_"+proc+"_"+str(maxconsts)+"C"+suffix+"_LHCO.h5", "r+")
    data = dict()
    hlvs = dict()
    #Get event
    event = infile["events/"+str(n_event)]
    #Loop through jets, up to topN
    for n_jet in range(min(len(event.keys()), int(topN))):
      #Add jet to event
      data.update({n_jet: event["jet"+str(n_jet)+"/constituents"][()]})
      hlvs.update({n_jet: (event["jet"+str(n_jet)+"/hlvs"][()] - hlv_means)/hlv_stds})
    return data, hlvs


def get_anom_event(n_event, hlv_means, hlv_stds, infile = None, n_const = None):
    if(infile is None): infile = h5py.File(dataDir+"Signal_"+str(maxconsts)+"C_"+proc+"_"+str(maxconsts)+"C"+suffix+"_LHCO.h5", "r+")
    data = dict()
    hlvs = dict()
    #Get event
    event = infile["events/"+str(n_event)]
    #Loop through jets, up to topN
    for n_jet in range(min(len(event.keys()), int(topN))):
      #Add jet to event
      data.update({n_jet: event["jet"+str(n_jet)+"/constituents"][()]})
      hlvs.update({n_jet: (event["jet"+str(n_jet)+"/hlvs"][()] - hlv_means)/hlv_stds})
    return data, hlvs

def get_loss(kld, nll):
  return 0.25*kld + nll


def ktd(c1, c2):
  power=1
  return np.minimum(np.power(c1[0], 2*power), np.power(c2[0], 2*power))*(np.square(c1[1] - c2[1]) + np.square(c1[2] - c2[2]))

def min_kt(jet1, jet2):
  minkt = 0
  ktds = np.zeros(shape=[len(jet1), len(jet2)])
  for i in range(len(jet1)):
    for j in range(len(jet2)):
      ktds[i][j] = ktd(jet1[i], jet2[j])
  for i in range(len(jet1)):
    idxs = np.unravel_index(np.argmin(ktds, axis=None), ktds.shape)
    minkt += ktds[idxs]
    ktds = np.delete(np.delete(ktds, idxs[0], 0), idxs[1], 1)
  return minkt


def min_kt_batch(jet1_b, jet2_b):
  minkt_list = np.zeros(len(jet1_b))
  for b in range(len(jet1_b)):
    jet1 = jet1_b[b]
    jet2 = jet2_b[b]
    minkt = 0
    ktds = np.zeros(shape=[len(jet1), len(jet2)])
    for i in range(len(jet1)):
      for j in range(len(jet2)):
        ktds[i][j] = ktd(jet1[i], jet2[j])
    for i in range(len(jet1)):
      idxs = np.unravel_index(np.argmin(ktds, axis=None), ktds.shape)
      minkt += ktds[idxs]
      ktds = np.delete(np.delete(ktds, idxs[0], 0), idxs[1], 1)
    minkt_list[b] = minkt
  return minkt_list




def get_batches(data, batch_size):
    n_batches = int(len(data)/batch_size)
    batched_data = []
    if(n_batches > 0):
      batched_data = np.empty(shape=[n_batches, batch_size, np.shape(data)[1], np.shape(data)[2]])
      for batch in range(n_batches):
        batched_data[batch] = data[batch*batch_size:(batch+1)*batch_size]
      #print(np.shape(batched_data))
    return batched_data

def get_hlv_batches(data, batch_size):
    n_batches = int(len(data)/batch_size)
    batched_data = []
    if(n_batches > 0):
      batched_data = np.empty(shape=[n_batches, batch_size, np.shape(data)[1]])
      for batch in range(n_batches):
        batched_data[batch] = data[batch*batch_size:(batch+1)*batch_size]
      #print(np.shape(batched_data))
    return batched_data


def plot_image(x, y, epoch, setname="", i=None):
  plt.clf()
  #if i is None: i = np.random.randint(0, 10)
  truex = x.transpose()[1]
  truey = x.transpose()[2]
  truer = 100*(np.square(x.transpose()[0])) + 10.

  plotx = y.transpose()[1]
  ploty = y.transpose()[2]
  plotr = 100*(np.square(y.transpose()[0])) + 10.
  #plotx = y.transpose()[1]
  #ploty = y.transpose()[2]
  #plotr = 1000*(np.square(y.transpose()[0])) + 10.

  #print(truer)
  #print(plotr)

  
  #plt.axhline(y=0.5, linestyle='--', linewidth=1, color='k', alpha=0.5)
  #plt.axvline(x=0.5, linestyle='--', linewidth=1, color='k', alpha=0.5)
  plt.axhline(y=0., linestyle='--', linewidth=1, color='k', alpha=0.5)
  plt.axvline(x=0., linestyle='--', linewidth=1, color='k', alpha=0.5)
  #plt.scatter(truex, truey, s=truer, marker='o', alpha=0.5, label='true')
  #plt.scatter(plotx, ploty, s=plotr, marker='o', alpha=0.5, label='reco')
  plt.scatter(truex, truey, s=truer, marker='o', alpha=0.5, label='input')
  plt.scatter(plotx, ploty, s=plotr, marker='o', alpha=0.5, label='output')
  plt.xlabel(r'$\eta$')
  plt.ylabel(r'$\phi$')
  #plt.xlim(0., 1.)
  #plt.ylim(0., 1.)
  plt.xlim(-1., 1.)
  plt.ylim(-1., 1.)
  plt.title(setname+' Set, Epoch '+str(epoch))
  plt.legend()
  
  #fig.canvas.flush_events()
  #plt.show()
  plt.savefig("rnd_plots/image_"+proc+"_ep"+str(epoch)+".png")
  #plt.pause(1e-6)

def get_batch_major(data):
  out = np.empty(batch_size, 12, 3)
  for b in range(batch_size()):
    #for i in range(len(data)):
    for i in range(data):
      tmp_data = data[i].detach().numpy()
      out[b][i] = tmp_data[b]
  return out




def get_mse(t1, t2): 
  mse=0
  for i in range(len(t1)):
    for j in range(len(t1[i])):
      mse += np.square(t2[i][j] - t1[i][j])
  mse /= len(t1)*len(t1[0])  
  return mse

"""
def train(epoch):
  train_loss = 0
  total_batches = 0
  for n_c in data_train.keys():
    if(int(n_c.replace("C","")) in const_list):
      start = time.time()
      n_jets = len(data_train[n_c])
      total_batches += n_jets/batch_size
      batch_counter = 0
      while(batch_counter < n_jets and batch_counter < max_batches*batch_size):
        batch_start = time.time()
        batch_step = min(batch_size, n_jets - batch_counter)
        data = torch.tensor(data_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        hlvs = torch.tensor(hlvs_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        data = Variable(data.transpose(0, 1))
        optimizer.zero_grad()
        #kld_loss, nll_loss, loss, y_mean = model(data, hlvs)
        kld_loss, nll_loss, loss, y_mean = model(data)
        loss.backward()
        optimizer.step()

        #grad norm clipping, only in pytorch version >= 1.10
        nn.utils.clip_grad_norm(model.parameters(), clip)

        train_loss += loss.data
        batch_counter += batch_step

      
  print('====> Epoch: {} Average loss: {:.4f}'.format(
    epoch, train_loss / (max_batches)))
    #epoch, train_loss / (total_batches)))
  train_loss /= total_batches
  return train_loss.cpu().numpy()
"""

def train(epoch):
  train_loss = 0
  total_batches = 0
  for n_c in data_train.keys():
    if(int(n_c.replace("C","")) in const_list):
      start = time.time()
      n_jets = len(data_train[n_c])
      total_batches += n_jets/batch_size
      batch_counter = 0
      avgs = avg_jets[n_c]
      while(batch_counter < n_jets and batch_counter < max_batches*batch_size):
        batch_start = time.time()
        batch_step = min(batch_size, n_jets - batch_counter)
        data = torch.tensor(data_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        hlvs = torch.tensor(hlvs_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        data = Variable(data.transpose(0, 1))
        optimizer.zero_grad()
        kld_loss, nll_loss, loss, y_mean = model(data, hlvs, avgs, kl_weight)
        #kld_loss, nll_loss, loss, y_mean = model(data)
        loss.backward()
        optimizer.step()

        #grad norm clipping, only in pytorch version >= 1.10
        nn.utils.clip_grad_norm(model.parameters(), clip)

        train_loss += loss.data
        batch_counter += batch_step

      
  print('====> Epoch: {} Average loss: {:.4f}'.format(
    epoch, train_loss / (max_batches)))
    #epoch, train_loss / (total_batches)))
  train_loss /= total_batches



  #Print example jet

  #if(epoch%plot_every == 0):
  #  in_jet = torch.tensor(data_train["10C"][np.random.randint(0, 100)]).unsqueeze(0).float().transpose(0, 1).cuda()
  #  _, _, _, out_jet = model(in_jet)
  # 

  #  in_jet = in_jet.squeeze(1).cpu().numpy()
  #  out_jet = out_jet.squeeze(1).cpu().numpy()



  #  #minkt = min_kt(in_jet, out_jet)
  #  #print("min kt", minkt)
  #  #min_kt_trend.append(minkt)

  #  plot_image(in_jet, out_jet, epoch)

  return train_loss.cpu().numpy()

















def test(epoch):
  """uses test data to evaluate 
  likelihood of the model"""
  v_loss = 0
  total_batches = 0
  for n_c in data_val.keys():
    if(int(n_c.replace("C","")) in const_list):
      n_jets = len(data_val[n_c])
      total_batches += n_jets/batch_size
      batch_counter = 0
      avgs = avg_jets[n_c]
      #for i in range(len(batched_val)):
      while(batch_counter < n_jets):
        batch_step = min(batch_size, n_jets - batch_counter)
        data = torch.tensor(data_val[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        hlvs = torch.tensor(hlvs_val[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        data = Variable(data.transpose(0, 1))
        kld_loss, nll_loss, loss, y_mean = model(data, hlvs, avgs, kl_weight)
        #kld_loss, nll_loss, loss, y_mean = model(data)
        v_loss += loss.data
        batch_counter += batch_step

  v_loss /= total_batches 
  return v_loss

def evaluate_training(hlv_means, hlv_stds, n_events, epoch=""):
  """uses test data to evaluate 
  likelihood of the model"""
  kld_losses_sum = []
  kld_losses_max = []
  minkt_losses_sum = []
  minkt_losses_max = []
  kldmkt_losses_sum = []
  kldmkt_losses_max = []
  vecs_save = []
  print("Evaluating Training")
  for n_c in data_train.keys():
    if(int(n_c.replace("C","")) in const_list):
      start = time.time()
      n_jets = len(data_train[n_c])
      batch_counter = 0
      avgs = avg_jets[n_c]
      while(batch_counter < n_jets and batch_counter < max_batches*batch_size):
        batch_start = time.time()
        batch_step = min(batch_size, n_jets - batch_counter)
        data = torch.tensor(data_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        hlvs = torch.tensor(hlvs_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        vecs = torch.tensor(vecs_train[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        data = Variable(data.transpose(0, 1))
        #kld_loss, _, _, y_mean = model(data)
        kld_loss, _, _, y_mean = model(data, hlvs, avgs, kl_weight)
        #minkt = min_kt_batch(data.cpu().transpose(0, 1).numpy(), y_mean.cpu().transpose(0, 1).numpy())
        #minkt = 0 
        minkt = np.zeros(len(kld_loss)) 
        loss_kld = 4*kld_loss.data.cpu().numpy()
        loss_minkt = minkt
        loss_kldmkt = 4*kld_loss.data.cpu().numpy() + minkt
        kld_losses_sum = np.concatenate((kld_losses_sum, loss_kld))
        minkt_losses_sum = np.concatenate((minkt_losses_sum, loss_minkt))
        kldmkt_losses_sum = np.concatenate((kldmkt_losses_sum, loss_kldmkt))


        if(len(vecs_save) == 0): vecs_save = vecs.cpu().numpy()
        else: vecs_save = np.concatenate((vecs_save, vecs.cpu().numpy()), axis=0)

        batch_counter += batch_step


  kld_losses_max = kld_losses_sum
  minkt_losses_max = minkt_losses_sum
  kldmkt_losses_max = kldmkt_losses_sum

  return kld_losses_sum, kld_losses_max, minkt_losses_sum, minkt_losses_max, kldmkt_losses_sum, kldmkt_losses_max, vecs_save

def evaluate_validation(hlv_means, hlv_stds, n_events, epoch=""):
  """uses test data to evaluate 
  likelihood of the model"""
  kld_losses_sum = []
  kld_losses_max = []
  minkt_losses_sum = []
  minkt_losses_max = []
  kldmkt_losses_sum = []
  kldmkt_losses_max = []
  vecs_save = []
  print("Evaluating Validation")

  for n_c in data_val.keys():
    if(int(n_c.replace("C","")) in const_list):
      start = time.time()
      n_jets = len(data_val[n_c])
      batch_counter = 0
      avgs = avg_jets[n_c]
      while(batch_counter < n_jets and batch_counter < max_batches*batch_size):
        batch_start = time.time()
        batch_step = min(batch_size, n_jets - batch_counter)
        data = torch.tensor(data_val[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        hlvs = torch.tensor(hlvs_val[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        vecs = torch.tensor(vecs_val[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        data = Variable(data.transpose(0, 1))
        #kld_loss, _, _, y_mean = model(data)
        kld_loss, _, _, y_mean = model(data, hlvs, avgs, kl_weight)
        #minkt = min_kt_batch(data.cpu().transpose(0, 1).numpy(), y_mean.cpu().transpose(0, 1).numpy())
        #minkt = 0 
        minkt = np.zeros(len(kld_loss)) 
        loss_kld = 4*kld_loss.data.cpu().numpy()
        loss_minkt = minkt
        loss_kldmkt = 4*kld_loss.data.cpu().numpy() + minkt
        kld_losses_sum = np.concatenate((kld_losses_sum, loss_kld))
        minkt_losses_sum = np.concatenate((minkt_losses_sum, loss_minkt))
        kldmkt_losses_sum = np.concatenate((kldmkt_losses_sum, loss_kldmkt))

        if(len(vecs_save) == 0): vecs_save = vecs.cpu().numpy()
        else: vecs_save = np.concatenate((vecs_save, vecs.cpu().numpy()), axis=0)


        batch_counter += batch_step


  kld_losses_max = kld_losses_sum
  minkt_losses_max = minkt_losses_sum
  kldmkt_losses_max = kldmkt_losses_sum

  return kld_losses_sum, kld_losses_max, minkt_losses_sum, minkt_losses_max, kldmkt_losses_sum, kldmkt_losses_max, vecs_save

def evaluate_anom(hlv_means, hlv_stds, n_events, epoch=""):
  """uses test data to evaluate 
  likelihood of the model"""
  kld_losses_sum = []
  kld_losses_max = []
  minkt_losses_sum = []
  minkt_losses_max = []
  kldmkt_losses_sum = []
  kldmkt_losses_max = []
  vecs_save = []
  print("Evaluating Anom")

  for n_c in data_anom.keys():
    if(int(n_c.replace("C","")) in const_list):
      start = time.time()
      n_jets = len(data_anom[n_c])
      batch_counter = 0
      avgs = avg_jets[n_c]
      while(batch_counter < n_jets and batch_counter < max_batches*batch_size):
        batch_start = time.time()
        batch_step = min(batch_size, n_jets - batch_counter)
        data = torch.tensor(data_anom[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        hlvs = torch.tensor(hlvs_anom[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        vecs = torch.tensor(vecs_anom[n_c][batch_counter:batch_counter+batch_step]).float().cuda()
        data = Variable(data.transpose(0, 1))
        #kld_loss, _, _, y_mean = model(data)
        kld_loss, _, _, y_mean = model(data, hlvs, avgs, kl_weight)
        #minkt = min_kt_batch(data.cpu().transpose(0, 1).numpy(), y_mean.cpu().transpose(0, 1).numpy())
        minkt = np.zeros(len(kld_loss)) 
        loss_kld = 4*kld_loss.data.cpu().numpy()
        loss_minkt = minkt
        loss_kldmkt = 4*kld_loss.data.cpu().numpy() + minkt
        kld_losses_sum = np.concatenate((kld_losses_sum, loss_kld))
        minkt_losses_sum = np.concatenate((minkt_losses_sum, loss_minkt))
        kldmkt_losses_sum = np.concatenate((kldmkt_losses_sum, loss_kldmkt))

        if(len(vecs_save) == 0): vecs_save = vecs.cpu().numpy()
        else: vecs_save = np.concatenate((vecs_save, vecs.cpu().numpy()), axis=0)

        batch_counter += batch_step


  kld_losses_max = kld_losses_sum
  minkt_losses_max = minkt_losses_sum
  kldmkt_losses_max = kldmkt_losses_sum

  return kld_losses_sum, kld_losses_max, minkt_losses_sum, minkt_losses_max, kldmkt_losses_sum, kldmkt_losses_max, vecs_save


#hyperparameters
#x_dim = 28
x_dim = 3
hlv_dim = 10
#hlv_dim =12
#h_dim = 100
#z_dim = 16
n_layers =  1
n_epochs  = 500
#n_epochs  = 5
clip = 10
learning_rate = 1e-5
l2_norm = 0
batch_size = 256
#batch_size = 1
#batch_size = 10
seed = 128
print_every = 100
save_every = 1
eval_every = 1
max_batches = 500
plot_every = 20



plot_images = False
save_images = False



min_kt_trend = []
roc_kld_trend = []
roc_minkt_trend = []
roc_kldmkt_trend = []
roc_kldmkt_v_trend = []
roc_kld_v_trend = []

m_corrs = []
pt_corrs = []


print("CUDA Available:", torch.cuda.is_available())
print("CUDA Version:", torch.version.cuda)
print("CUDA Device:", torch.cuda.get_device_name())
torch.cuda.set_device(cuda_idx)
print("CUDA Index:", torch.cuda.current_device())


#manual seed
torch.manual_seed(seed)
#plt.ion()

data_train, hlvs_train, hlv_means, hlv_stds, n_train_events, avg_jets, vecs_train, masses_train = get_data()
data_val, hlvs_val, n_val_events, vecs_val, masses_val = get_val_data(hlv_means, hlv_stds)
data_anom, hlvs_anom, n_anom_events, vecs_anom, masses_anom = get_anom_data(hlv_means, hlv_stds)




print(data_train.keys())
#const_list = [8]
#const_list = range(3, maxconsts+1)
const_list = range(4, maxconsts+1)
#const_list = [40]

losses_train = []
losses_val = []
model = VRNN(x_dim, hlv_dim, h_dim, z_dim, n_layers, train_hlvs)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=l2_norm)
do_train=True
#do_train=False


#state_dict = torch.load('saves/vrnn_state_dict_111.pth')
#model.load_state_dict(state_dict)
if(load):
  fn = 'saves/vrnn_state_dict_'+rocname+'_epoch_100.pth'
  state_dict = torch.load(fn, map_location=lambda storage, loc: storage)
  model.load_state_dict(state_dict)



if(do_train):
  for epoch in range(1, n_epochs + 1):
    l_train = 0
    l_val = 0
    l_anom = 0 
    #training + testing
    start_time = time.time()
    l_train = train(epoch)
    l_val = test(epoch)
    print("Time: ", time.time() - start_time)
    print("Memory Usage: ", torch.cuda.memory_allocated('cuda:0')/1e9)
    #losses_val.append(test(epoch))
    losses_train.append(l_train)
    losses_val.append(l_val)
    
    """
    plt.figure(2)
    plt.clf()
    plt.plot(losses_train, label="train")
    plt.plot(losses_val, label="val")
    plt.plot(losses_anom, label="anom")
    plt.legend()
    plt.yscale("log")
    #fig.canvas.flush_events()
    #plt.show()
    plt.savefig("plots/training_"+rocname+".png")
    #plt.pause(1e-6)
    """
    if(epoch % eval_every == 0):
      eval_time = time.time()
      scores_normal_sum, scores_normal_max, scores_minkt_normal_sum, scores_minkt_normal_max, scores_kldmkt_normal_sum, scores_kldmkt_normal_max, vecs_t = evaluate_training(hlv_means, hlv_stds, n_train_events, epoch=str(epoch))
      scores_val_sum, scores_val_max, scores_minkt_val_sum, scores_minkt_val_max, scores_kldmkt_val_sum, scores_kldmkt_val_max, vecs_v = evaluate_validation(hlv_means, hlv_stds, n_val_events, epoch=str(epoch))
      scores_anom_sum, scores_anom_max, scores_minkt_anom_sum, scores_minkt_anom_max, scores_kldmkt_anom_sum, scores_kldmkt_anom_max, vecs_a = evaluate_anom(hlv_means, hlv_stds, n_anom_events, epoch=str(epoch))
      print("Evaluating Time:", time.time()-eval_time)

      scores_normal_sum = 1-np.exp(np.multiply(scores_normal_sum, -1))
      scores_val_sum = 1-np.exp(np.multiply(scores_val_sum, -1))
      scores_anom_sum = 1-np.exp(np.multiply(scores_anom_sum, -1))
      scores_normal_max = 1-np.exp(np.multiply(scores_normal_max, -1))
      scores_val_max = 1-np.exp(np.multiply(scores_val_max, -1))
      scores_anom_max = 1-np.exp(np.multiply(scores_anom_max, -1))

      scores_minkt_normal_sum = 1-np.exp(np.multiply(scores_minkt_normal_sum, -1))
      scores_minkt_val_sum = 1-np.exp(np.multiply(scores_minkt_val_sum, -1))
      scores_minkt_anom_sum = 1-np.exp(np.multiply(scores_minkt_anom_sum, -1))
      scores_minkt_normal_max = 1-np.exp(np.multiply(scores_minkt_normal_max, -1))
      scores_minkt_val_max = 1-np.exp(np.multiply(scores_minkt_val_max, -1))
      scores_minkt_anom_max = 1-np.exp(np.multiply(scores_minkt_anom_max, -1))

      scores_kldmkt_normal_sum = 1-np.exp(np.multiply(scores_kldmkt_normal_sum, -1))
      scores_kldmkt_val_sum = 1-np.exp(np.multiply(scores_kldmkt_val_sum, -1))
      scores_kldmkt_anom_sum = 1-np.exp(np.multiply(scores_kldmkt_anom_sum, -1))
      scores_kldmkt_normal_max = 1-np.exp(np.multiply(scores_kldmkt_normal_max, -1))
      scores_kldmkt_val_max = 1-np.exp(np.multiply(scores_kldmkt_val_max, -1))
      scores_kldmkt_anom_max = 1-np.exp(np.multiply(scores_kldmkt_anom_max, -1))


      scores_all_sum = scores_normal_sum
      scores_all_sum = np.append(scores_all_sum, scores_anom_sum)
      scores_all_val_sum = scores_val_sum
      scores_all_val_sum = np.append(scores_all_val_sum, scores_anom_sum)
      scores_all_max = scores_normal_max
      scores_all_max = np.append(scores_all_max, scores_anom_max)
      scores_all_val_max = scores_val_max
      scores_all_val_max = np.append(scores_all_val_max, scores_anom_max)

      scores_minkt_all_sum = scores_minkt_normal_sum
      scores_minkt_all_sum = np.append(scores_minkt_all_sum, scores_minkt_anom_sum)
      scores_minkt_all_val_sum = scores_minkt_val_sum
      scores_minkt_all_val_sum = np.append(scores_minkt_all_val_sum, scores_minkt_anom_sum)
      scores_minkt_all_max = scores_minkt_normal_max
      scores_minkt_all_max = np.append(scores_minkt_all_max, scores_minkt_anom_max)
      scores_minkt_all_val_max = scores_minkt_val_max
      scores_minkt_all_val_max = np.append(scores_minkt_all_val_max, scores_minkt_anom_max)

      scores_kldmkt_all_sum = scores_kldmkt_normal_sum
      scores_kldmkt_all_sum = np.append(scores_kldmkt_all_sum, scores_kldmkt_anom_sum)
      scores_kldmkt_all_val_sum = scores_kldmkt_val_sum
      scores_kldmkt_all_val_sum = np.append(scores_kldmkt_all_val_sum, scores_kldmkt_anom_sum)
      scores_kldmkt_all_max = scores_kldmkt_normal_max
      scores_kldmkt_all_max = np.append(scores_kldmkt_all_max, scores_kldmkt_anom_max)
      scores_kldmkt_all_val_max = scores_kldmkt_val_max
      scores_kldmkt_all_val_max = np.append(scores_kldmkt_all_val_max, scores_kldmkt_anom_max)

      labels = np.append(np.zeros(len(scores_normal_sum)), np.ones(len(scores_anom_sum)))
      labels_val = np.append(np.zeros(len(scores_val_sum)), np.ones(len(scores_anom_sum)))

      fpr_sum, tpr_sum, _ = roc_curve(labels, scores_all_sum)
      fpr_v_sum, tpr_v_sum, _ = roc_curve(labels_val, scores_all_val_sum)
      fpr_max, tpr_max, _ = roc_curve(labels, scores_all_max)
      fpr_v_max, tpr_v_max, _ = roc_curve(labels_val, scores_all_val_max)

      fpr_minkt_sum, tpr_minkt_sum, _ = roc_curve(labels, scores_minkt_all_sum)
      fpr_minkt_v_sum, tpr_minkt_v_sum, _ = roc_curve(labels_val, scores_minkt_all_val_sum)
      fpr_minkt_max, tpr_minkt_max, _ = roc_curve(labels, scores_minkt_all_max)
      fpr_minkt_v_max, tpr_minkt_v_max, _ = roc_curve(labels_val, scores_minkt_all_val_max)

      fpr_kldmkt_sum, tpr_kldmkt_sum, _ = roc_curve(labels, scores_kldmkt_all_sum)
      fpr_kldmkt_v_sum, tpr_kldmkt_v_sum, _ = roc_curve(labels_val, scores_kldmkt_all_val_sum)
      fpr_kldmkt_max, tpr_kldmkt_max, _ = roc_curve(labels, scores_kldmkt_all_max)
      fpr_kldmkt_v_max, tpr_kldmkt_v_max, _ = roc_curve(labels_val, scores_kldmkt_all_val_max)

      roc_auc_sum = auc(fpr_sum, tpr_sum) # compute area under the curve
      roc_auc_v_sum = auc(fpr_v_sum, tpr_v_sum) # compute area under the curve
      roc_auc_max = auc(fpr_max, tpr_max) # compute area under the curve
      roc_auc_v_max = auc(fpr_v_max, tpr_v_max) # compute area under the curve

      roc_auc_minkt_sum = auc(fpr_minkt_sum, tpr_minkt_sum) # compute area under the curve
      roc_auc_minkt_v_sum = auc(fpr_minkt_v_sum, tpr_minkt_v_sum) # compute area under the curve
      roc_auc_minkt_max = auc(fpr_minkt_max, tpr_minkt_max) # compute area under the curve
      roc_auc_minkt_v_max = auc(fpr_minkt_v_max, tpr_minkt_v_max) # compute area under the curve

      roc_auc_kldmkt_sum = auc(fpr_kldmkt_sum, tpr_kldmkt_sum) # compute area under the curve
      roc_auc_kldmkt_v_sum = auc(fpr_kldmkt_v_sum, tpr_kldmkt_v_sum) # compute area under the curve
      roc_auc_kldmkt_max = auc(fpr_kldmkt_max, tpr_kldmkt_max) # compute area under the curve
      roc_auc_kldmkt_v_max = auc(fpr_kldmkt_v_max, tpr_kldmkt_v_max) # compute area under the curve



      np.save("plots/"+rocname+"/roc/fpr_train_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_sum)
      np.save("plots/"+rocname+"/roc/tpr_train_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_sum)
      np.save("plots/"+rocname+"/roc/fpr_v_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_v_sum)
      np.save("plots/"+rocname+"/roc/tpr_v_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_v_sum)
      np.save("plots/"+rocname+"/roc/fpr_train_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_max)
      np.save("plots/"+rocname+"/roc/tpr_train_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_max)
      np.save("plots/"+rocname+"/roc/fpr_v_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_v_max)
      np.save("plots/"+rocname+"/roc/tpr_v_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_v_max)

      np.save("plots/"+rocname+"/roc/fpr_minkt_train_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_minkt_sum)
      np.save("plots/"+rocname+"/roc/tpr_minkt_train_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_minkt_sum)
      np.save("plots/"+rocname+"/roc/fpr_minkt_v_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_minkt_v_sum)
      np.save("plots/"+rocname+"/roc/tpr_minkt_v_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_minkt_v_sum)
      np.save("plots/"+rocname+"/roc/fpr_minkt_train_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_minkt_max)
      np.save("plots/"+rocname+"/roc/tpr_minkt_train_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_minkt_max)
      np.save("plots/"+rocname+"/roc/fpr_minkt_v_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_minkt_v_max)
      np.save("plots/"+rocname+"/roc/tpr_minkt_v_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_minkt_v_max)

      np.save("plots/"+rocname+"/roc/fpr_kldmkt_train_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_kldmkt_sum)
      np.save("plots/"+rocname+"/roc/tpr_kldmkt_train_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_kldmkt_sum)
      np.save("plots/"+rocname+"/roc/fpr_kldmkt_v_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_kldmkt_v_sum)
      np.save("plots/"+rocname+"/roc/tpr_kldmkt_v_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_kldmkt_v_sum)
      np.save("plots/"+rocname+"/roc/fpr_kldmkt_train_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_kldmkt_max)
      np.save("plots/"+rocname+"/roc/tpr_kldmkt_train_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_kldmkt_max)
      np.save("plots/"+rocname+"/roc/fpr_kldmkt_v_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_kldmkt_v_max)
      np.save("plots/"+rocname+"/roc/tpr_kldmkt_v_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_kldmkt_v_max)

      np.save("plots/"+rocname+"/scores/scores_normal_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_normal_sum)
      np.save("plots/"+rocname+"/scores/scores_val_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_val_sum)
      np.save("plots/"+rocname+"/scores/scores_anom_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_anom_sum)
      np.save("plots/"+rocname+"/scores/scores_normal_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_normal_max)
      np.save("plots/"+rocname+"/scores/scores_val_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_val_max)

      np.save("plots/"+rocname+"/scores/scores_minkt_normal_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_normal_sum)
      np.save("plots/"+rocname+"/scores/scores_minkt_val_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_val_sum)
      np.save("plots/"+rocname+"/scores/scores_minkt_anom_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_anom_sum)
      np.save("plots/"+rocname+"/scores/scores_minkt_normal_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_normal_max)
      np.save("plots/"+rocname+"/scores/scores_minkt_val_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_val_max)

      np.save("plots/"+rocname+"/scores/scores_kldmkt_normal_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_normal_sum)
      np.save("plots/"+rocname+"/scores/scores_kldmkt_val_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_val_sum)
      np.save("plots/"+rocname+"/scores/scores_kldmkt_anom_sum_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_anom_sum)
      np.save("plots/"+rocname+"/scores/scores_kldmkt_normal_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_normal_max)
      np.save("plots/"+rocname+"/scores/scores_kldmkt_val_max_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_val_max)

      roc_kld_trend.append(roc_auc_sum)
      roc_minkt_trend.append(roc_auc_minkt_sum)
      roc_kldmkt_trend.append(roc_auc_kldmkt_sum)
      roc_kldmkt_v_trend.append(roc_auc_kldmkt_v_sum)


      v_scores = scores_kldmkt_val_max
      v_pt = []
      v_m = []
      for i in range(len(v_scores)):
        v_pt.append(vecs_v[i][0]) 
        v_m.append(vecs_v[i][3]) 
      
      corr = np.corrcoef([v_m, v_pt, v_scores])
      
      m_corrs.append(corr[0][2])
      pt_corrs.append(corr[1][2])



      """
      plt.clf()
      plt.plot(roc_kld_trend, label="KLD")
      plt.plot(roc_minkt_trend, label="MinKt")
      plt.plot(roc_kldmkt_trend, label="KLD+MinKt")
      plt.plot(roc_kldmkt_v_trend, label="KLD+MinKt Validation", linestyle='--', color='black')
      plt.ylim(0., 1.05)
      plt.axhline(0.1, linestyle=':', color='lightgray')
      plt.axhline(0.2, linestyle=':', color='lightgray')
      plt.axhline(0.3, linestyle=':', color='lightgray')
      plt.axhline(0.4, linestyle=':', color='lightgray')
      plt.axhline(0.5, linestyle='--', color='gray')
      plt.axhline(0.6, linestyle=':', color='lightgray')
      plt.axhline(0.7, linestyle=':', color='lightgray')
      plt.axhline(0.8, linestyle=':', color='lightgray')
      plt.axhline(0.9, linestyle=':', color='lightgray')
      plt.title(str(kl_weight)+" "+str(hlv_dim)+"_"+str(z_dim))
      plt.legend()
      ##plt.show()
      #if(load):
      #  plt.savefig("rnd_plots/aucs_train_"+proc+"_"+sample+"_"+str(maxconsts)+"_ep"+str(epoch)+".png")
      else:
        plt.savefig("rnd_plots/hyperparams/aucs_train_scratch_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".png")
      """


      """
      plt.clf()
      fig, ax1 =  plt.subplots() #AUC plot
      ax1.plot(roc_kldmkt_trend, label="Training Set ROC AUC", color='tab:blue')
      ax1.plot(roc_kldmkt_v_trend, label="Validation Set ROC AUC", linestyle='--', color='black')
      ax1.set_ylim(0., 1.0)
      ax1.set_xlabel("Epoch")
      ax1.set_ylabel("ROC AUC", color='tab:blue')
      ax1.tick_params(axis='y', labelcolor='tab:blue')
      ax1.axhline(0.1, linestyle=':', color='lightgray')
      ax1.axhline(0.2, linestyle=':', color='lightgray')
      ax1.axhline(0.3, linestyle=':', color='lightgray')
      ax1.axhline(0.4, linestyle=':', color='lightgray')
      ax1.axhline(0.5, linestyle='--', color='gray')
      ax1.axhline(0.6, linestyle=':', color='lightgray')
      ax1.axhline(0.7, linestyle=':', color='lightgray')
      ax1.axhline(0.8, linestyle=':', color='lightgray')
      ax1.axhline(0.9, linestyle=':', color='lightgray')
      ax1.set_title(proc)
      ax2 = ax1.twinx()
      ax2.set_ylabel("Correlation Coefficient", color='tab:orange')
      ax2.plot(m_corrs, label="Mass Correlation", color='tab:orange')
      ax2.plot(pt_corrs, label="pT Correlation", color='tab:red')
      ax2.set_ylim(-1., 1.)
      ax2.tick_params(axis='y', labelcolor='tab:orange')



      plt.legend()
      #if(epoch >= 5):
      #  plt.savefig("test_corrs.png")
      #  sys.exit(0)
      #plt.savefig("rnd_plots/correlations_adv_ep"+str(epoch)+".png")
      plt.savefig("rnd_plots/aucs_train_scratch_BVar_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".png")
      """



      #np.save("rnd_plots/hyperparams/roc_t_train_scratch_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".npy", roc_kldmkt_trend)
      #np.save("rnd_plots/hyperparams/roc_v_train_scratch_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".npy", roc_kldmkt_v_trend)



      np.save("rnd_plots/3p/roc_t_train_scratch_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".npy", roc_kldmkt_trend)
      np.save("rnd_plots/3p/roc_v_train_scratch_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".npy", roc_kldmkt_v_trend)
      np.save("rnd_plots/3p/m_corrs_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".npy", m_corrs)
      np.save("rnd_plots/3p/pt_corrs_"+proc+"_"+sample+"_Top"+topN+"_"+str(maxconsts)+"_ep"+str(epoch)+"_"+str(kl_weight).replace(".","p")+"_"+str(h_dim)+"_"+str(z_dim)+".npy", pt_corrs)

      


      print("Epoch: "+str(epoch)+": Max "+str(maxconsts)+" Constituents Evaluation:")
      print("Training AUC:", roc_auc_sum)
      print("Validation AUC:", roc_auc_v_sum)

    #saving model
    if epoch % save_every == 0:
      fn = 'saves/vrnn_state_dict_'+rocname+'_epoch_'+str(epoch)+'.pth'
      torch.save(model.state_dict(), fn)
      print('Saved model to '+fn)


  #Save training losses
  np.save("plots/"+rocname+"/losses_train_"+rocname+".png", losses_train)
  np.save("plots/"+rocname+"/losses_val_"+rocname+".png", losses_val)
